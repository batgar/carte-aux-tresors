export function sum(a, b) {
    return a + b;
}

const ORIENTATIONORDER = ["N","O","S","E"]

export function createMap(constructObject) {
    let { carte, montains=[], treasur=[] } = constructObject
    let map = []
    // creating dots to create an empty map
    map = Array(carte.height).fill('.').map(x => Array(carte.width).fill('.'))
    // replacing dots by montains
    montains.forEach(position => {
        map[position[1]][position[0]] = "M"
    });
    // placing treasur
    treasur.forEach(gold => {
        map[gold.y][gold.x] = gold.nb
    });
    return map
}

export function moveAdventurer(map, adventurer) {
    
    // Set adventurer keys if not defined
    if (adventurer["nbTreasur"] === undefined) {
        adventurer.nbTreasur = 0
    }
    if (adventurer["instruction"] === undefined) {
        adventurer.instruction = ""
    }

    if (adventurer.instruction == "") {
        return {
            map: map,
            adventurer: adventurer
        }
    } else {
        // Get instruction and change adventurer for his next instruction
        let instruction = adventurer.instruction.charAt(0)
        adventurer.instruction = adventurer.instruction.slice(1)
        
        // Apply instruction
        switch (instruction) {
            case "A":
                switch (adventurer.orientation) {
                    case "N":
                        if (adventurer.y > 0) {
                            if (map[adventurer.y - 1][adventurer.x] !== "M") {
                                adventurer.y -= 1 
                                if (typeof(map[adventurer.y][adventurer.x]) === "number" && map[adventurer.y][adventurer.x] > 0) {
                                    adventurer.nbTreasur += 1
                                    map[adventurer.y][adventurer.x] = map[adventurer.y][adventurer.x] -= 1 
                                }
                        }}
                            
                        break;
                    case "S":
                        if (adventurer.y < map.length - 1) {
                            if (map[adventurer.y + 1][adventurer.x] !== "M") {
                                adventurer.y += 1
                                if (typeof(map[adventurer.y][adventurer.x]) === "number" && map[adventurer.y][adventurer.x] > 0) {
                                    adventurer.nbTreasur += 1
                                    map[adventurer.y][adventurer.x] = map[adventurer.y][adventurer.x] -= 1 
                                }
                        }}
                        break;
                    case "E":
                        if (adventurer.x + 1 < map[0].length - 1) { 
                            if (map[adventurer.y][adventurer.x + 1] !== "M") {
                                adventurer.x += 1
                                if (typeof(map[adventurer.y][adventurer.x]) === "number" && map[adventurer.y][adventurer.x] > 0) {
                                    adventurer.nbTreasur += 1
                                    map[adventurer.y][adventurer.x] = map[adventurer.y][adventurer.x] -= 1 
                                }
                        }}
                        break;
                    case "O":
                        if (adventurer.x > 0) {
                            if (map[adventurer.y][adventurer.x - 1] !== "M") {
                                adventurer.x -= 1
                                if (typeof(map[adventurer.y][adventurer.x]) === "number" && map[adventurer.y][adventurer.x] > 0) {
                                    adventurer.nbTreasur += 1
                                    map[adventurer.y][adventurer.x] = map[adventurer.y][adventurer.x] -= 1 
                                }
                        }}
                        break;
                    
                    default:
                        break;
                }
                break;

            case "G":
                if (adventurer.orientation === "E") {
                    adventurer.orientation = ORIENTATIONORDER[0]
                } else {
                    adventurer.orientation = ORIENTATIONORDER[ORIENTATIONORDER.indexOf(adventurer.orientation) + 1]
                }
                break;
        
            case "D":
                if (adventurer.orientation === "N") {
                    adventurer.orientation = ORIENTATIONORDER[3]
                } else {
                    adventurer.orientation = ORIENTATIONORDER[ORIENTATIONORDER.indexOf(adventurer.orientation) - 1]
                }
                break;
            default:
                break;
        }
    }
    console.log({
        map: map,
        adventurer: adventurer
    })
    return {
        map: map,
        adventurer: adventurer
    }
}

export function play(game) {
    if (game.adventurer.instruction === "") {
        return game
    } else {
        return play(moveAdventurer(game.map,game.adventurer))
    }
}