import { sum, createMap, moveAdventurer, play } from "../src/index.js"

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
  });

it('should create an empty map', () => {
  let constructObject = {
    carte: { 
      width: 3,
      height: 2
    }
  }
  expect(createMap(constructObject)).toEqual(
    [ [ '.', '.', '.' ], [ '.', '.', '.' ] ]
  )
});


it('should create a map with montains on position [0,0] [2,1]', () => {
  let constructObject = {
    carte: { 
      width: 3,
      height: 2
    },
    montains: [[0,0],[2,1]]
  }
  expect(createMap(constructObject)).toEqual(
    [ [ 'M', '.', '.' ], [ '.', '.', 'M' ] ]
  )
});

it('should create a map with montains on position [0,0] [2,1] and two treasur on [2,0] and [1,1]', () => {
  let constructObject = {
    carte: { 
      width: 3,
      height: 2
    },
    montains: [[0,0],[2,1]],
    treasur: [
      {
        x:2,
        y:0,
        nb:2
      },
      {
        x:1,
        y:1,
        nb:1
      }
    ]
  }
  expect(createMap(constructObject)).toEqual(
    // On pose uniquement le nombre de trésor(s) déposé sur la carte
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ]
  )
});

it('should return a brand new map and and adventurer object', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "S",
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "S",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map, without the treasur on [1,1] and adventurer object with a different position and one treasur', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 1,
      y: 0,
      orientation: "S",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 0, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 1,
        y: 1,
        orientation: "S",
        instruction: "",
        nbTreasur: 1
      }
    }
  )
});

// Test adventurer can't mouve on montains
it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 1,
      y: 0,
      orientation: "O",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 1,
        y: 0,
        orientation: "O",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "N",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "N",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', '.' ], [ '.', '.', 'M' ] ], 
    {
      name: "Fred",
      x: 2,
      y: 0,
      orientation: "S",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', '.' ], [ '.', '.', 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 2,
        y: 0,
        orientation: "S",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', '.' ], [ '.', '.', 'M' ] ], 
    {
      name: "Fred",
      x: 1,
      y: 1,
      orientation: "E",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', '.' ], [ '.', '.', 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 1,
        y: 1,
        orientation: "E",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});


// Test adventurer can't mouve outside of the map
it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 1,
      y: 0,
      orientation: "N",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 1,
        y: 0,
        orientation: "N",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "S",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "S",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "O",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "O",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return a map with the adventurer with the same position', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, '.' ] ], 
    {
      name: "Fred",
      x: 2,
      y: 1,
      orientation: "E",
      instruction: "A"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, '.' ] ],
      adventurer: {
        name: "Fred",
        x: 2,
        y: 1,
        orientation: "E",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

// Test change orientation "Gauche"
it('should return the same map with the adventurer with an North orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "E",
      instruction: "G"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "N",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return the same map with the adventurer with an West orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "N",
      instruction: "G"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "O",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return the same map with the adventurer with an South orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "O",
      instruction: "G"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "S",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return the same map with the adventurer with an East orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "S",
      instruction: "G"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "E",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

// Test change orientation "Droite"
it('should return the same map with the adventurer with an South orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "E",
      instruction: "D"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "S",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return the same map with the adventurer with an East orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "N",
      instruction: "D"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "E",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return the same map with the adventurer with an North orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "O",
      instruction: "D"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "N",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

it('should return the same map with the adventurer with an West orientation', () => {
  expect(moveAdventurer(
    [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ], 
    {
      name: "Fred",
      x: 0,
      y: 1,
      orientation: "S",
      instruction: "D"
    }
  )).toEqual(
    {
      map : [ [ 'M', '.', 2 ], [ '.', 1, 'M' ] ],
      adventurer: {
        name: "Fred",
        x: 0,
        y: 1,
        orientation: "O",
        instruction: "",
        nbTreasur: 0
      }
    }
  )
});

// Tests with multiple instructions
let constructObject = {
  carte: { 
    width: 3,
    height: 4
  },
  montains: [[1,0],[2,1]],
  treasur: [
    {
      x:0,
      y:3,
      nb:2
    },
    {
      x:1,
      y:3,
      nb:3
    }
  ]
}
let adventurer = {
  name: "Lara",
  x: 1,
  y: 1,
  orientation: "S",
  instruction: "AADADAGGA"
}
it('should return the end of the game, when the adventurer has no instruction left', () => {
  expect(play({map :createMap(constructObject),adventurer: adventurer})).toEqual(
    {
      map : [
        [ '.', 'M', '.' ],
        [ '.', '.', 'M' ],
        [ '.', '.', '.' ],
        [ 0, 2, '.' ]
      ],
      adventurer: {
        name: "Lara",
        x: 0,
        y: 3,
        orientation: "S",
        instruction: "",
        nbTreasur: 3
      }
    }
  )
});